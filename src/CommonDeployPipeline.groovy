import com.cloudbees.plugins.credentials.common.StandardUsernamePasswordCredentials
import com.cloudbees.plugins.credentials.CredentialsProvider
import hudson.util.Secret

import java.net.URLConnection
import java.io.InputStream
import org.yaml.snakeyaml.Yaml

def runPipeline() {

    def commonDeployer = new CommonDeployer()
    def jmeterTest = new JMeterTestpipeline()
    def postmanTest = new PostmanNewmanPipeline()
    def smokeTest = new SmokeTestPipeline()
	 def tfHome
    //def soapuiTest = new SoapUITestPipeline()
    def deployData = []
    
    def functionalTestReportName
    def performanceTestReportName

    def jobName = "${JOB_BASE_NAME}"
        .toLowerCase()
        .replace(" ", "-")
        .replace(".net", "dotnet")
		
	def deployEnv = jobName.substring(jobName.lastIndexOf("-") + 1)
    print deployEnv
    jobName = jobName.substring(0, jobName.lastIndexOf("-deploy"))
    println jobName

    def buildToDeploy = params["BuildToDeploy"].toLowerCase()
    
    def baseUrl,scmUrl, scmBrowseUrl, scmBrowseBranch, scmBrowseBuildFileUrl, buildFile, buildFileData, scmBrowseTFPath, scmBrowseConfigFileUrl, tfConfigFile, deployRoleArnMatch, git_commit_short_head
    try {
      // Get the Browse SCM url
      scmUrl = scm.getUserRemoteConfigs()[0].getUrl()
      scmBrowseUrl = scmUrl.replaceAll(/https:\/\/[^@]*@([^\/]*)\/scm\/([^\/]*)\/([^\.]*)\.git/, 'https://$1/projects/$2/repos/$3/browse')
      scmBrowseBranch = scm.branches[0].name.replaceAll(/^\*\//, '')

      print "scmBrowseBranch : ${scmBrowseBranch}"
      // Get the Terraform config path
      scmBrowseBuildFileUrl = "${scmBrowseUrl}/build.yaml"

     // baseUrl = "https://bitbucket.org/moulima07/${jobName}"
     baseUrl = "https://raw.githubusercontent.com/Zebra/${jobName}"
      


    } catch (e) {
      print "ExceptionMessage"
      print e.getMessage()
    }

    
    node () {
		tfHome = tool 'Terraform'
        currentBuild.result = 'SUCCESS'
        try {
            stage ("Deployment Pull") {

                // Get deployment package
                //commonDeployer.deployment_artifact_pull(jobName)
                //deployData = readYaml file: "deployment.yaml"
                //println deployData
                cleanWs()
                // Doing This becouse Artifactory not intergrated for deployment package strategy
                checkout scm

                dir("${workspace}") {
                    println "Setting Variables"
                   
				   sshagent (credentials: ['5004637f-046b-49e0-a413-93cbb7b7a954'])
				   
				//	withCredentials([sshUserPrivateKey(credentialsId: '5004637f-046b-49e0-a413-93cbb7b7a954', usernameVariable: 'GIT_ACCESS_USERID')])
					{
                        println "Setting Variables1"
                            buildFileData = readYaml file: "build.yaml"
                        //  def git_commit_hash_head = sh (script: "git rev-parse --verify HEAD", returnStdout: true).trim()
                           
 
                           // scmBrowseBuildFileUrl = "${baseUrl}/raw/${git_commit_hash_head}/build.yaml"
							//scmBrowseBuildFileUrl="${baseUrl}/${scmBrowseBranch}/build.yaml?token=ArWDukYlBewTQ4V5H0_u3jho8DQ3sJLoks5cIhe5wA%3D%3D"	
                                                         
						  // buildFile = commonDeployer.getSingleFileFromGit(scmBrowseBuildFileUrl, scmBrowseBranch)

                           // buildFileData = new Yaml().load(buildFile)

                            scmBrowseTFPath = buildFileData.deployment.terraform_path

                            // Get the configuration file
                            //scmBrowseConfigFileUrl = "${scmBrowseUrl}/${scmBrowseTFPath}/configurations/${params["ConfigFile"]}"
                           // scmBrowseConfigFileUrl = "${baseUrl}/raw/${git_commit_hash_head}/terraform/configurations/${params["ConfigFile"]}"
							//scmBrowseConfigFileUrl="${baseUrl}/${scmBrowseBranch}/terraform/configurations/${params["ConfigFile"]}?token=ArWDuiS3Vm90MYceNeqcHMj3mSRZ1A06ks5cIhjmwA%3D%3D"
                          //  tfConfigFile = commonDeployer.getSingleFileFromGit(scmBrowseConfigFileUrl, scmBrowseBranch)

                           

                            git_commit_short_head = sh (script: "git rev-parse --short=6 HEAD", returnStdout: true).trim()

                            if (buildToDeploy == 'latest') {
                              //query = 'items.find({"repo":{"$eq":"devops-deployments"},"path":{"$match": "' +  reponame + '/*"}})'
                              print "buildToDeploy : 0.1-${git_commit_short_head}"
                            } else {
                                git_commit_short_head = buildToDeploy.split(" -- ").last().split("_").last()
                                print "buildToDeploy : 0.1-${git_commit_short_head}"
                                //query = 'items.find({"repo":{"$eq":"devops-deployments"},"path":{"$match": "' + reponame + '/*/' + buildToDeploy + '"}})'
                            }

                            print "git_commit_short_head : ${git_commit_short_head}" 
							 
                        
                    }
                }

            }

            // We are not doing true parallel due to limitations in visualizaions:
            // see https://issues.jenkins-ci.org/browse/JENKINS-38442

            // Pre build tasks
            stage ("Artifact Pull") {
                

                println buildFileData.apigee 
                if(buildFileData.apigee != null) {
                    println "In Artifact Pull Stage"
                    commonDeployer.muleonprem_artifact_pull(git_commit_short_head,scmBrowseBranch,buildFileData)
                }
            }

            // Deployment Scan
            stage ("Deployment Scan") {
                commonDeployer.terraform_scan("${workspace}/${scmBrowseTFPath}")
            }

            // Plan or Apply
             stage ("Plan / Apply") {
                 commonDeployer.terraform_apply(
                         params["ApplyChanges"],
                         "${workspace}/${scmBrowseTFPath}",
                         params["ConfigFile"],
                         jobName,
                         git_commit_short_head,     //deployData.git_hash.take(6),
                         (buildFileData.components.collect {cur -> return cur.name
                         }).join(","), // Create a comma seperated string of build names
                         (buildFileData.components.collect {
                           cur -> return (cur.buildtype == "docker") ? cur.build_artifact : cur.build_artifact
                         }).join(","), // Create a comma seperated string of build artifacts
                         buildFileData.version, //deployData.version,
                         scmBrowseBranch
                 )
             } 

           // Run Smoke Test
           stage ("Smoke Test") {
             if (buildFileData.smoke_test ) {
               smokeTest.smoketest_run()
             } else {
               println("Skipping Smoke Test")
             }
           }

           // Run Functional test
          stage ("Functional Test Run") {
             if (buildFileData.postman_newman_path ) {
                functionalTestReportName = "Postman_20Report"
                postmanTest.postmantest_run(buildFileData.postman_newman_path,deployEnv)
             } else if(buildFileData.soapui_path){
                functionalTestReportName = "testReport"
                soapuiTest.soapuitest_run(buildFileData.soapui_path,deployEnv)
             } else {
               println("No functional test path found. Functional Test was not run.")
             }
           } 

         /*  // Run Performance Test
           stage ("Performance Test Run") {
             if (buildFileData.jmeter_path ) {
                performanceTestReportName = "Jmeter_20Summary_20Report"
                jmeterTest.jmetertest_run(buildFileData.jmeter_path,deployEnv)
             } else {
               println("No Performance test path found. Performance Test was not run.")
             }
           }
*/
        } catch (e) {
            if (e instanceof org.jenkinsci.plugins.scriptsecurity.sandbox.RejectedAccessException) {
                throw e
            }
            currentBuild.result = 'FAILURE'
            println "ERROR Detected:"
            println e.getMessage()
            def sw = new StringWriter()
            def pw = new PrintWriter(sw)
            e.printStackTrace(pw)
            println sw.toString()
        } finally {

           /* def commonEmail = new CommonEmail()
            def mailContent = ""

            //Adding test links to mail body for mule base project only as of now
            if (buildFileData.muleonprem != null || buildFileData.apigee != null) {
                mailContent =  
                """
                 <div class='content'>
                   <h1>Functional Tests</h1>
                   <table class="console">
                     <p> Postman Functional Test Results available at</p>
                     <a href="${BUILD_URL}${functionalTestReportName}/">${BUILD_URL}${functionalTestReportName}/</a>
                    </table>
                 </div>
                 """ +
                """
                 <div class='content'>
                   <h1>Performance Tests</h1>
                  <table class="console">
                     <p>Jmeter Performance Test Results available at</p> <br>
                     <p>Performance Report</p> <br>
                     <a href="${BUILD_URL}performance/">${BUILD_URL}performance/</a> <br>
                     <p>Summary Report</p> <br>
                     <a href="${BUILD_URL}${performanceTestReportName}/">${BUILD_URL}${performanceTestReportName}/</a>
                    </table>
                 </div>
                 """
            }



            commonEmail.sendDeployToJobSubmitter(mailContent)*/

        }
    } // node (k8slabel)
  

}

return this
